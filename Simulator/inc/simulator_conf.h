#ifndef __INCLUDE_SIMULATOR_CONFIG_H_
#define __INCLUDE_SIMULATOR_CONFIG_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

//=======================================================================//
//= Macro definition.                                                   =//
//=======================================================================//
#define SIMULATOR_DEFAULT_SCREEN_WIDTH  (128)
#define SIMULATOR_DEFAULT_SCREEN_HEIGHT (64)
#define SIMULATOR_DEFAULT_PIXEL_SCALE   (2)
#define SIMULATOR_BACKGROUND_COLOR      (0xD1FA00)
#define SIMULATOR_FOREGROUND_COLOR      (0x5F6F03)
//#define SIMULATOR_BACKGROUND_COLOR      (0x13181F)
//#define SIMULATOR_FOREGROUND_COLOR      (0x0AFDFD)

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif // __INCLUDE_SIMULATOR_CONFIG_H_
