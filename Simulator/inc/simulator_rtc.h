#ifndef __INCLUDE_CLASS_SIMULATOR_RTC_TIMER_H_
#define __INCLUDE_CLASS_SIMULATOR_RTC_TIMER_H_

#include "simulator_timer.h"

class simulator_rtc : public simulator_timer
{
private:
    bool                            m_is_update;

protected:
    virtual void                    on_timer_triggered(void);

public:
    explicit                        simulator_rtc(void);
    virtual                         ~simulator_rtc(void);
    bool                            is_update(void);
};

#endif // __INCLUDE_CLASS_SIMULATOR_RANDOM_DATA_H_
