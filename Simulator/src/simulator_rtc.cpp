#include "simulator_rtc.h"
#include "console_log.h"

simulator_rtc::simulator_rtc(void)
: simulator_timer(1000, true)
, m_is_update(false)
{

}

simulator_rtc::~simulator_rtc(void)
{

}

void simulator_rtc::on_timer_triggered(void)
{
    m_is_update = true;
}

bool simulator_rtc::is_update(void)
{
    bool rtc_update = m_is_update;
    m_is_update = false;
    return rtc_update;
}

