#ifndef _INCLUDE_RESOURCE_H_
#define _INCLUDE_RESOURCE_H_

#include "SGUI_Typedef.h"
#ifndef _SIMPLE_GUI_DEMO_INNER_CHS_
#include "DemoResource_ASCII.h"
#else
#ifdef _SIMPLE_GUI_ENCODE_TEXT_
#include "DemoResource_UTF8.h"
#else
#include "DemoResource_GB2312.h"    
#endif // _SIMPLE_GUI_ENCODE_TEXT_
#endif

extern const SGUI_FONT_RES GB2312_FZXS12;


#endif // _INCLUDE_RESOURCE_H_
