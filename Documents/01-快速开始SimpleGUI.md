# 快速开始SimpleGUI  
---  
## 1. 了解SimpleGUI  
#### 1.1. SimpleGUI概述  
&emsp;&emsp;SimpleGUI是一个针对于单色显示屏设计和开发的GUI接口，提供了基本的点、线、矩形、圆等基本图形的绘制功能、文字和位图的绘制以及列表、滚动条、进度条等常用组件的绘制与控制功能。  
&emsp;&emsp;SimpleGUI的出发点，是在一个单色显示器上，以尽可能少的消耗、尽可能多且直观的表达需要的内容，因此，SimpleGUI抛弃了诸如图层、阴影、抗锯齿等高级的、复杂的操作，力求以简单快捷、易于操作的方式，使开发者尽快实现自己需要的功能需求。 同时在满足基本绘图需求的前提下，SimpleGUI还提供了一套被称为HMI的交互引擎，用于统合用户交互、数据处理和屏幕内容绘制与更新，提供了一种简明、易组织、易拓展、低消耗的交互系统。  
  
#### 1.2. 获取SimpleGUI  
&emsp;&emsp;SimpleGUI目前托管在码云（Gitee）开源平台上，您可以通过Git工具，从码云上将SimpleGUI的全部代码和资料同步到本地，如果您不想使用Git工具，也可以在[SimpleGUI工程页面](https://gitee.com/Polarix/simplegui)中点击“克隆/下载”按钮，在弹出的窗口中点击“下载ZIP”按钮下载整个工程的压缩包文件。  
&emsp;&emsp;SimpleGUI的主目录结构和说明如下：  
  
|目录名|功能|  
|:- |:- |  
|DemoProc|SimpGUI的演示代码|  
|DemoProject|SimpleGUI的演示工程|  
|Documents|关于SimpleGUI的一些简要说明文档|  
|GUI|SimpleGUI的代码实现部分|  
|HMI|SimpleGUI的HMI模型实现部分|  
|Simulator|基于SDL2的模拟器|  
  
## 2. 编译和使用模拟器  
#### 2.1. 什么是模拟器  
&emsp;&emsp;SimpleGUI使用SDL2构建了一个简单的模拟环境，除可以演示、预览SimpleGUI外，还可以作为脱离硬件平台进行图形界面开发的辅助工具使用。配合SimpleGUI的低耦合性接移植口定义，使用模拟器开发的用户界面处理源码，几乎额可以无缝的移植到预期的硬件平台上。  
&emsp;&emsp;在初次接触SimpleGUI时，您可能尚未选定或准备好相应的硬件平台。鉴于这种情况，您可以从模拟器环境开始，初步了解SimpleGUI的显示效果以及代码架构。  
  
#### 2.2. 使用CMake快速完成模拟器的编译  
&emsp;&emsp;在使用CMake编译模拟器示例之前，需要先修改一下CMakeLists.txt文件，找到SDL_DIR变量，将变量的值修改为SDL库的实际路径。
``` text
set(SDL_DIR D:/Works/dev/libs/SDL/SDL2-2.28.3/x86_64-w64-mingw32)
```
> 将D:/Works/dev/libs/SDL/SDL2-2.28.3/x86_64-w64-mingw32修改为SDL2库的实际部署路径。

&emsp;&emsp;然后在当前仓库目录下创建一个新文件夹build，然后再build目录中打开控制台，然后执行以下命令：
``` cmd
cmake.exe ../CMakeLists.txt -B ./ -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug -DUSE_CHS_TEXT=True
```
> - 要正常再控制台中使用CMake，需要将CMake添加到系统的PATH环境变量中。
> - USE_CHS_TEXT=True参数是启用中文演示资源，如果不添加此项目，演示程序将使用英文显示。

#### 2.3. 使用Codeblocks打开模拟器环境  
&emsp;&emsp;SimpleGUI的模拟器也提供了基于Code::Blocks的图形化IDE环境，可以方便的进行调试、断点跟踪等等。
&emsp;&emsp;进入Simulator目录，打开Simulator.cbp工程文件，如果用户下载的是nosetup（绿色版）的Code::Blocks，需要手动建立关联才能直接用双击的方式打开cbp文件，否则，用户只能先打开Code::Blocks，然后从Code::Blocks中执行打开操作以打开工程文件。  
&emsp;&emsp;点击工具栏上的编译按钮或按快捷键Ctrl+F9，开始编译模拟环境演示工程。  
![01-编译工程](images/01/01.png)

&emsp;&emsp;编译完成，显示无错误和警告，现在点击工具栏上的运行按钮或按快捷键Ctrl+F10就可以看到模拟环境的运行效果了。  
![02-运行效果](images/01/02.png)
  
#### 2.4. 模拟不同型号和外观的屏幕效果  
&emsp;&emsp;默认情况下，模拟器中虚拟显示屏幕的配色方案为黄底黑字的LCD点阵显示屏，为最大程度上模拟真实情况下的视觉效果，方便创建和调试GUI元素，VirtualSDK的LCD面板可以通过修改配置定义修改颜色和尺寸。  
&emsp;&emsp;打开模拟器工程，修改Simulator\inc\simulator_conf.h文件，这里这里是针对模拟器屏幕参数的宏定义，意义如下：  
```c++  
#define SIMULATOR_DEFAULT_SCREEN_WIDTH  (128)
#define SIMULATOR_DEFAULT_SCREEN_HEIGHT (64)
#define SIMULATOR_DEFAULT_PIXEL_SCALE   (2)
#define SIMULATOR_BACKGROUND_COLOR      (0xD1FA00)
#define SIMULATOR_FOREGROUND_COLOR      (0x5F6F03) 
```  
&emsp;&emsp;前景色和背景色的颜色定义，为RGB888格式：  
  
|成员/变量名|功能|  
|:- |:- |  
|SIMULATOR_DEFAULT_SCREEN_WIDTH|模拟器屏幕宽度|  
|SIMULATOR_DEFAULT_SCREEN_HEIGHT|模拟器屏幕高度|  
|SIMULATOR_BACKGROUND_COLOR|模拟器屏幕背景色，RGB888格式|  
|SIMULATOR_FOREGROUND_COLOR|模拟器屏幕前景色，RGB888格式|  
  
&emsp;&emsp;模拟器默认状态下模拟的是黄底黑字的12864LCD显示屏，如果要模拟其他屏幕的显示效果，可以使用一张要模拟屏幕的照片，使用取色工具获取屏幕背景、像素以及边框颜色的RGB值，比如淘宝上的照片，就可以直接使用。  
![03-实物照片](images/01/03.png)
&emsp;&emsp;然后分别修改SIMULATOR_BACKGROUND_COLOR和SIMULATOR_FOREGROUND_COLOR宏定义的内容，重新编译后，即可看到效果，此时已经模拟为黑底蓝字的OLED显示屏。  
![04-预览效果](images/01/04.png)
  
&emsp;&emsp;此外，还可以修改模拟的屏幕尺寸，例如我们想要模拟19264的屏幕，分别修改HorizontalPixelNumber和VerticalPixelNumbe的值为192和64。  
```c++  
#define SIMULATOR_DEFAULT_SCREEN_WIDTH  (192)
#define SIMULATOR_DEFAULT_SCREEN_HEIGHT (64)
```  
&emsp;&emsp;然后修改演示程序代码DemoProc/src/DemoProc.c文件中InitializeHMIEngineObj函数中对屏幕设备尺寸的设定，如果这里的尺寸不跟随模拟LCD的尺寸变化而修改，运行时的显示区域将可能不正确，显示效果出现异常。  
```c++  
/* Initialize display size. */  
g_stDeviceInterface.stSize.iWidth = 192;  
g_stDeviceInterface.stSize.iHeight = 64;  
```  
&emsp;&emsp;然后重新编译工程并运行。  
![05-预览效果](images/01/05.png)

&emsp;&emsp;至此，您已经可以通过VirtualSDK，模拟SimpleGUI在您预期的设备上的显示效果，此外您还可以通过键盘，和Demo程序进行交互，查看更多组件的显示和动作效果。  
  
### 3. 联系开发者  
&emsp;&emsp;首先，感谢您对SimpleGUI的赏识与支持。  
&emsp;&emsp;虽然最早仅仅作为一套GUI接口库使用，但我最终希望SimpleGUI能够为您提供一套完整的单色屏GUI及交互设计解决方案，如果您有新的需求、提议亦或想法，欢迎在以下地址留言，或加入[QQ交流群799501887](https://jq.qq.com/?_wv=1027&k=5ahGPvK)留言交流。  
>SimpleGUI@开源中国：https://www.oschina.net/p/simplegui  
>SimpleGUI@码云：https://gitee.com/Polarix/simplegui  
  
&emsp;&emsp;本人并不是全职的开源开发者，依然有工作及家庭的琐碎事务要处理，所以对于大家的需求和疑问反馈的可能并不及时，多有怠慢，敬请谅解。  
&emsp;&emsp;最后，再次感谢您的支持。  
